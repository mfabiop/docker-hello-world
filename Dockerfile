# Dockerfile to create Wintek Backend app
FROM node:8.11.3

RUN apt-get update &&\
 apt-get install -y mysql-client

ENV HOME=/home/node
WORKDIR $HOME/docker-hello-world
COPY package*.json ./
RUN npm install --silent --progress=false
COPY . .
CMD ["npm", "start"]
