const express = require('express');
const app = express();

app.use('/user', require('./user'));

app.listen(process.env.PORT);
