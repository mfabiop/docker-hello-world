const database = require('./database');
const express = require('express');
const router = express.Router();


router.get('/', function(req, res) {
  database.User.findAll({raw : true}).then((users) => {
    res.status(200).send(users);
  }).catch((err) => {
    console.log(err);
    res.status(400).end();
  })
});

router.post('/', function(req, res) {
  var username = req.query.username;
  var birthday = req.query.birthday;
  database.User.create({username, birthday}).then(() => {
    res.status(201).end();
  }).catch((err) => {
    console.log(err);
    res.status(400).end();
  })
})

module.exports = router;
